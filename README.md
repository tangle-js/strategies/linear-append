# @tangle/linear-append

A strategy for tracking a set over time, when it matters when a certain thing was added to a Set, and when it was removed

## Example Usage

This is mainly used with `@tangle/strategy` to compose higher order strategies, and `@tangle/reduce` to use it, but here's an example of using it stand alone

```js
const LinearAppend = require('@tangle/linear-append')

const linearAppend = LinearAppend()

const input = [
  { key: [111, '@mix'], value: 'what shall we have for dinner?' },
  { key: [115, '@mix'], value: 'Keen, I would like one without onion plz' }
  { key: [112, '@ben'], value: 'PIZZA!' },
}

const T = linearAppend.mapFromInput(input)
// => {
//   '111@mix': 'what shall we have for dinner?',
//   '115@mix': 'Keen, I would like one without onion plz'
//   '112@ben': 'PIZZA!',
// }
```

The raw transformations look like this:
```js
{
  [key]: value
}
```

where

* `key` *String* - a unique key used to later determine ordering in linear output
    * NOTE the human input is provided as an Array so you can provide multiple inputs which helps for tie-breaking
* `value` *String* - the value to be appended

```js
linearAppend.mapToOutput(T)
// => [
//   'what shall we have for dinner?',
//   'PIZZA!',
//   'Keen, I would like one without onion plz'
// ]
```

## API

### `LinearAppend(opts) => linearAppend`

Instantiates a strategy, `linearAppend`.

`opts` *Object* is an optional object where you can specify:
- `keyPattern` *String* (optional)
    - a JSON schema pattern string which is used to validate keys
    - Note that the Array of values in the human input is concatenated into a String key, and this is what is validated
    - e.g. `'^@\w+$'` would make only keys like `@mixmix` valid
    - default: `'^.+$'`
- `valueSchema` *Object* (optional)
    - a JSON schema object used for validating the values passed in.
    - default: `{ type: 'string', required: true }`


### `linearAppend.concat(A, B) => C`

### `linearAppend.identity() => I`

returns "identity transformation"

### `linearAppend.mapFromInput(input) => T`

Takes a human friendly (descriptive) `input` and
returns a transformation `T` which satisfies the
change requested in `input.

Format of input:
```js
[
  { key: OrderingKey, value: AppendValue },
  { key: OrderingKey, value: AppendValue },
  ...
]
```
where:
- `OrderingKey` is an Array which will be concatenated and compared to other values for ordering
- `AppendValue` is some value you want to append. Can be anything that passes your `valueSchema`


### `linearAppend.mapToOutput(T) => t`

Takes a transformation `T` and returns an output state `t`, which is more
"human readable"

Format of output `t`:

```js
[
  AppendValue,
  AppendValue,
  ...
]

### `linearAppend.isValid(T) => Boolean`

### `linearAppend.schema`

Access the JSON schema that `isValid` was built with.

### `linearAppend.isConflict() => False`
### `linearAppend.isValidMerge() => True`
### `linearAppend.merge(graph, mergeNode, field) => T`
where:
- `graph` is a `@tangle/graph` instance
- `mergeNode` is the proposed merge-node
- `field` *String* contains the the data fields `node.data[field]`

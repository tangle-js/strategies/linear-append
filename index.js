const Validator = require('is-my-json-valid')

const IDENTITY = {}

module.exports = function LinearAppend (opts = {}) {
  const {
    keyPattern = '^.+$',
    valueSchema = { type: 'string', required: true }
  } = opts

  const schema = {
    type: 'object',
    patternProperties: {
      [keyPattern]: valueSchema
    },
    additionalProperties: false
  }

  /*
   gets the new state from the current state + an input (change)
  */
  function mapFromInput (input, currentTips) {
    return input.reduce((acc, { key, value }) => {
      acc[key.join('')] = value
      return acc
    }, {})
  }

  function mapToOutput (T) {
    return Object.entries(T)
      .sort((a, b) => {
        return (a[0] < b[0] ? -1 : +1)
      })
      .map(entry => entry[1])
  }
  /*
    transformations are of form:
    {
      String: { Integer: Integer }
    }
    e.g.
    {
      id: { seq: state }
    }
  */

  function concat (a, b) {
    Object.keys(a).forEach(key => {
      if (key in b) {
        if (a[key] !== b[key]) throw new Error('Cannot concat identical keys with different values')
      }
    })
    return { ...a, ...b }
  }

  // copied from simple-set
  function merge (graph, mergeNode, field) {
    // Build a set of the keys in the history of mergeNode
    const keyHistory = new Set(mergeNode.previous)
    for (const prevKey of mergeNode.previous) {
      for (const historyKey of graph.getHistory(prevKey)) {
        keyHistory.add(historyKey)
      }
    }
    // Concat all of the fields in the history together
    const concatHistory = Array.from(keyHistory).map((key) => {
      if (field in graph.getNode(key).data) return graph.getNode(key).data[field]
      else return IDENTITY
    }).reduce((a, b) => { return concat(a, b) })
    // Concat the merge node field as well.
    const mergeNodeField = field in mergeNode.data ? mergeNode.data[field] : IDENTITY
    return concat(concatHistory, mergeNodeField)
  }

  return {
    identity: () => IDENTITY,
    concat,
    mapFromInput,
    mapToOutput,
    schema,
    isValid: Validator(schema, { verbose: true }),
    isConflict () { return false },
    isValidMerge () { return true },
    merge
  }
}

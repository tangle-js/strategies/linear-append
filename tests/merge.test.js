const test = require('tape')
const Strategy = require('../')
const tangle = require('@tangle/test')

const {
  identity,
  isConflict,
  isValidMerge,
  merge
} = Strategy()

const toKey = arr => arr.reverse().join('--')

test('merging', t => {
  const chereseFeedId = '@cherese5mnWKkJTgCIpqOsA4JeJd9Oz2gmv6rojQeXU=.ed25519'
  const mixFeedId = '@mixVv6k5mnWKkJTgCIpqOsA4JeJd9Oz2gmv6rojQeXU=.ed25519'

  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'linear' })
  const graph = buildGraph(`
     A-->B-->D
     A-->C-->D
   `)
  const mergeNode = graph.getNode('D')
  t.equal(isConflict(graph, ['B', 'C']), false, 'no conflict')
  t.equal(isValidMerge(graph, mergeNode), true, 'merge is always fine')
  // merge

  t.deepEqual(merge(buildGraph(`
     A-->B-->D
     A-->C-->D
   `), mergeNode, 'linear'), identity(), ' identity merge')

  const A = graph.getNode('A')
  A.data.linear[toKey([chereseFeedId, 10])] = 'what shall we have for lunch?'
  t.deepEqual(
    merge(graph, mergeNode, 'linear'),
    {
      [toKey([chereseFeedId, 10])]: 'what shall we have for lunch?'
    },
    'merge with root value'
  )

  const B = graph.getNode('B')
  B.data.linear[toKey([chereseFeedId, 11])] = 'how about pizza?'
  t.deepEqual(
    merge(graph, mergeNode, 'linear'),
    {
      [toKey([chereseFeedId, 10])]: 'what shall we have for lunch?',
      [toKey([chereseFeedId, 11])]: 'how about pizza?'
    },
    'merge one branch different feedIds'
  )

  const C = graph.getNode('C')
  C.data.linear[toKey([mixFeedId, 40])] = 'indian?'
  t.deepEqual(
    merge(graph, mergeNode, 'linear'),
    {
      [toKey([chereseFeedId, 10])]: 'what shall we have for lunch?',
      [toKey([chereseFeedId, 11])]: 'how about pizza?',
      [toKey([mixFeedId, 40])]: 'indian?'
    },
    'merge two branches'
  )

  mergeNode.data.linear[toKey([mixFeedId, 41])] = 'curizza?'
  t.deepEqual(
    merge(graph, mergeNode, 'linear'),
    {
      [toKey([chereseFeedId, 10])]: 'what shall we have for lunch?',
      [toKey([chereseFeedId, 11])]: 'how about pizza?',
      [toKey([mixFeedId, 40])]: 'indian?',
      [toKey([mixFeedId, 41])]: 'curizza?'
    },
    'merge all nodes'
  )

  /* failing case */
  mergeNode.data.linear[toKey([mixFeedId, 40])] = 'curizza?' // on-unique key
  t.throws(
    () => merge(graph, mergeNode, 'linear'),
    /identical keys with different values/,
    'merge fails with conflicting keys'
  )

  t.end()
})

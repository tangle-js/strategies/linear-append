const test = require('tape')
const LinearAppend = require('..')

test('identity', t => {
  const linearAppend = LinearAppend()
  const identity = linearAppend.identity()
  const mix1 = {
    '111@mix': 'what shall we have for dinner?'
  }

  const concat = linearAppend.concat
  t.deepEqual(mix1, concat(mix1, identity), 'simple identity')
  t.deepEqual(mix1, concat(identity, mix1), 'simple identity')
  t.end()
})

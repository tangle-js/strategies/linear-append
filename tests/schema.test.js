const test = require('tape')
const LinearAppend = require('..')

test('schema / isValid', t => {
  const { isValid } = LinearAppend()

  t.true(isValid({
    '111@mix': 'what shall we have for dinner?'
  }), 'correct valid (string value)')
  if (isValid.error) console.error(isValid.error)

  /* Invalid */
  t.false(isValid({ dog: 2 }), '{ dog: 2 } is invalid')
  t.false(isValid({ dog: null }), '{ dog: null } is invalid')
  t.false(isValid('dog'), '"dog" is invalid')
  t.false(isValid({
    '111@mix': {
      author: '@mix',
      text: 'what shall we have for dinner?'
    }
  }), 'object value is invalid')
  t.false(isValid({ dog: undefined }), '{ dog: undefined } is invalid')

  t.end()
})

test('schema / isValid (with opts)', t => {
  const { isValid } = LinearAppend({
    keyPattern: '^\\d+@\\w+',
    valueSchema: { type: 'object' }
  })

  t.true(isValid({
    '111@mix': {
      author: '@mix',
      text: 'what shall we have for dinner?'
    }
  }), 'correct key + value pattern pass')
  if (isValid.error) console.error(isValid.error)

  /* invalid */
  t.false(isValid({
    'mix@111': {
      author: '@mix',
      text: 'what shall we have for dinner?'
    }
  }), 'incorrect key fails')

  t.false(isValid({
    '111@mix': 'what shall we have for dinner?'
  }), 'incorrect value fails')

  t.end()
})

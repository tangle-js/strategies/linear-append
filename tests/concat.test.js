const test = require('tape')
const LinearAppend = require('..')

test('concat', t => {
  const linearAppend = LinearAppend()

  const mix = {
    '111@mix': 'what shall we have for dinner?',
    '115@mix': 'Keen, I would like one without onion plz'
  }
  const ben = {
    '112@ben': 'PIZZA!'
  }
  const expected = {
    '111@mix': 'what shall we have for dinner?',
    '115@mix': 'Keen, I would like one without onion plz',
    '112@ben': 'PIZZA!'
  }
  const actual = linearAppend.concat(mix, ben)
  t.deepEqual(actual, expected, 'simple concat')

  t.end()
})

test('concat (assosiativity)', t => {
  const linearAppend = LinearAppend()

  const mix1 = {
    '111@mix': 'what shall we have for dinner?'
  }
  const mix2 = {
    '115@mix': 'Keen, I would like one without onion plz'
  }
  const ben1 = {
    '112@ben': 'PIZZA!'
  }
  const expected = {
    '111@mix': 'what shall we have for dinner?',
    '115@mix': 'Keen, I would like one without onion plz',
    '112@ben': 'PIZZA!'
  }
  const concat = linearAppend.concat
  const left = concat(concat(mix1, mix2), ben1)
  const right = concat(mix1, concat(mix2, ben1))
  t.deepEqual(left, right, 'simple assiotive')
  t.deepEqual(left, expected, 'simple assiotive')

  t.deepEqual(concat(mix1, mix2), concat(mix2, mix1), 'simple commutativity')

  t.end()
})

test('concat break', t => {
  const linearAppend = LinearAppend()
  const mix1 = {
    '111@mix': 'what shall we have for dinner?'
  }
  const mix2 = {
    '111@mix': 'Keen, I would like one without onion plz'

  }
  t.throws(() => linearAppend.concat(mix1, mix2), /Cannot concat identical keys with different values/,
    'Identical keys, different values should throw an error')

  t.deepEqual(mix1, linearAppend.concat(mix1, mix1), 'If a key is duplicated with the same value its fine')

  t.end()
})

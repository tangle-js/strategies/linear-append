const test = require('tape')
const Strategy = require('@tangle/strategy')
const LinearAppend = require('..')

test('isValid', t => {
  t.true(Strategy.isValid(LinearAppend()), 'is valid according to @tangle/strategy')
  if (Strategy.isValid.error) console.log(Strategy.isValid.error)

  t.end()
})

test('mapFromInput', t => {
  const input = [
    { key: [111, '@mix'], value: 'what shall we have for dinner?' },
    { key: [115, '@mix'], value: 'Keen, I would like one without onion plz' },
    { key: [112, '@ben'], value: 'PIZZA!' }
  ]
  const linearAppend = LinearAppend()

  const actual = linearAppend.mapFromInput(input, [linearAppend.identity()])
  const expected = {
    '111@mix': 'what shall we have for dinner?',
    '115@mix': 'Keen, I would like one without onion plz',
    '112@ben': 'PIZZA!'
  }

  t.deepEqual(actual, expected, 'simple input to transform')
  t.end()
})

test('mapToOutput', t => {
  const linearAppend = LinearAppend()

  const transform = {
    '111@mix': 'what shall we have for dinner?',
    '115@mix': 'Keen, I would like one without onion plz',
    '112@ben': 'PIZZA!'
  }
  const actual = linearAppend.mapToOutput(transform)
  const expected = [
    'what shall we have for dinner?',
    'PIZZA!',
    'Keen, I would like one without onion plz'
  ]

  t.deepEqual(actual, expected, 'simple transform to output')
  t.end()
})
